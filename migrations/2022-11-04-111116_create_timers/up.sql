CREATE TABLE IF NOT EXISTS timers (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	title VARCHAR NOT NULL,
	duration INTERVAL NOT NULL,
	elapsed_before_start INTERVAL NOT NULL DEFAULT '0 seconds',
	start_time timestamp
);
