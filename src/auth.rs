use tonic::Request;
use std::io::{Error as IOError, ErrorKind as IOErrorKind};

static ALGORITHM: jsonwebtoken::Algorithm = jsonwebtoken::Algorithm::RS384;

#[derive(Debug, serde::Deserialize)]
pub struct TokenClaims {
    pub exp: usize,
    pub sub: String,
}

fn get_public_pem_sync() -> Result<Vec<u8>, IOError> {
    let key_env = std::env::var("USERS_TOKEN_PUBLIC_PEM")
        .map_err(|err| {
            IOError::new(IOErrorKind::NotFound, err)
        })?;
    let mut file = std::fs::File::open(key_env)?;
    let mut out = Vec::new();
    std::io::Read::read_to_end(&mut file, &mut out)?;
    Ok(out)
}

fn token_from_authorization_header(
    key: &jsonwebtoken::DecodingKey,
    header: &tonic::metadata::MetadataValue<tonic::metadata::Ascii>,
) -> Result<jsonwebtoken::TokenData<TokenClaims>, jsonwebtoken::errors::Error> {
    let token = header
        .to_str()
        .map(|header| header.split_once(' ').map_or(header, |v| v.1))
        .unwrap_or("");
    let token = jsonwebtoken::decode(token, &key, &jsonwebtoken::Validation::new(ALGORITHM))?;
    Ok(token)
}

fn get_decoding_key() -> tonic::Result<jsonwebtoken::DecodingKey> {
    let key_data = get_public_pem_sync()
        .map_err(|err| {
            std::eprintln!("Could not load users token public key: {err:?}");
            tonic::Status::internal("Internal error")
        })?;
    jsonwebtoken::DecodingKey::from_rsa_pem(&key_data)
        .map_err(|_| {
            std::eprintln!("Error whilst loading key");
            tonic::Status::internal("Internal error")
        })
}

pub fn insert_token_in_extensions(request: &mut Request<()>) -> tonic::Result<()>{
    let metadata = request.metadata();
    let auth_token = metadata
        .get("authorization")
        .map(|val| {
            token_from_authorization_header(&get_decoding_key()?, val).map_err(|err|{
                std::eprintln!("Parsing auth token failed: {err:?}");
                tonic::Status::unauthenticated("Invalid Authorization header")
            })
        })
        .transpose()? ;
    if let Some(v) = auth_token {
        request.extensions_mut().insert(v);
    };
    Ok(())
}

pub fn auth_token_from_request<T>(request: &Request<T>) -> Option<&jsonwebtoken::TokenData<TokenClaims>> {
    request.extensions().get()
}

pub fn user_from_request<T>(request: &Request<T>) -> Option<uuid::Uuid> {
    auth_token_from_request(request).map(|data| {
        uuid::Uuid::parse_str(&data.claims.sub).ok()
    }).flatten()
}
