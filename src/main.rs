pub(crate) mod eyesheet {
    pub mod chronch {
        tonic::include_proto!("eyesheet.chronch");
    }
}

pub const MIGRATIONS: diesel_migrations::EmbeddedMigrations = diesel_migrations::embed_migrations!();

pub(crate) mod schema;
mod auth;

use std::ffi::OsStr;

use chrono::NaiveDateTime;
use rdkafka::{producer::{FutureProducer, FutureRecord}, error::KafkaError, types::RDKafkaErrorCode};
use schema::timers;

use eyesheet::chronch::*;

use diesel::{prelude::*, PgConnection, r2d2::{Pool, ConnectionManager}, Insertable, data_types::PgInterval};
use diesel_migrations::MigrationHarness;
use dotenv::dotenv;

use chronch_server::{Chronch, ChronchServer};
use tonic::transport::Server;
use tonic::{Result as StatusResult, Request, Response};
use uuid::Uuid;
use futures_util::stream::{FuturesUnordered, TryStreamExt};

struct ChronchService {
    db_pool: Pool<ConnectionManager<PgConnection>>,
    kafka_producer: Option<FutureProducer>,
}

impl ChronchService {
    async fn notify_timer_change(&self, users: Option<Vec<Uuid>>) -> Result<(), KafkaError> {
        let Some(kafka_producer) = &self.kafka_producer else {
            return Ok(());
        };
        let payload = serde_json::to_vec(&serde_json::json!({
            "only_users": users,
        })).expect("should always serialize");

        let record = FutureRecord {
            topic: "timer-change",
            partition: None,
            payload: Some(&payload),
            key: None::<&()>,
            timestamp: None,
            headers: None
        };

        let timeout = std::time::Duration::from_secs_f64(2.0);
        let res = kafka_producer.send(record, timeout).await;
        res
            .map(|_| ())
            .map_err(|err| err.0)
            .or_else(|err| match err.rdkafka_error_code() {
                Some(RDKafkaErrorCode::QueueFull) => {
                    std::eprintln!("Queue was full whilst trying to send kakfka timer-change notification");
                    Ok(())
                },
                _ => Err(err),
            })
    }
}

#[tonic::async_trait]
impl Chronch for ChronchService {
    async fn timer_total(
        &self,
        request: Request<TimerTotalRequest>,
    ) -> StatusResult<Response<TimerTotalResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let (total, owner) = timers::table
            .find(Uuid::try_parse(&message.uuid).unwrap())
            .select((timers::duration, timers::owner))
            .get_result::<(PgInterval, Uuid)>(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let total = total.microseconds;

        should_allow_access(auth::user_from_request(&request), owner)
            .then_some(Response::new(TimerTotalResponse { total }))
            .ok_or(tonic::Status::permission_denied("User does not own this timer."))
    }

    async fn is_timer_paused(
        &self,
        request: Request<IsTimerPausedRequest>,
    ) -> StatusResult<Response<IsTimerPausedResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let (is_paused, owner) = timers::table
            .find(Uuid::try_parse(&message.uuid).unwrap())
            .select((timers::start_time.is_null(), timers::owner))
            .get_result(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        should_allow_access(auth::user_from_request(&request), owner)
            .then_some(Response::new(IsTimerPausedResponse { is_paused }))
            .ok_or(tonic::Status::permission_denied("User does not own this timer."))
    }

    async fn timer_remaining(
        &self,
        request: Request<TimerRemainingRequest>,
    ) -> StatusResult<Response<TimerRemainingResponse>> {
        let message = request.get_ref();

        let current_time = request_timestamp_or_now(request.extensions());

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let (remaining, owner) = timers::table
            .find(Uuid::try_parse(&message.uuid).unwrap())
            .select((timers::duration, timers::elapsed_before_start, timers::start_time, timers::owner))
            .get_result::<(PgInterval, PgInterval, Option<NaiveDateTime>, Uuid)>(&mut db_conn)
            .map(|(duration, elapsed_before_start, start_time, owner)| {
                let remaining = match start_time {
                    Some(start_time) =>  duration.microseconds - elapsed_before_start.microseconds - (current_time.naive_utc() - start_time).num_microseconds().unwrap(),
                    None => duration.microseconds - elapsed_before_start.microseconds,
                };
                (remaining, owner)
                })
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        should_allow_access(auth::user_from_request(&request), owner)
            .then_some(Response::new(TimerRemainingResponse { remaining }))
            .ok_or(tonic::Status::permission_denied("User does not own this timer."))
    }

    async fn timer_end(
        &self,
        request: Request<TimerEndRequest>,
    ) -> StatusResult<Response<TimerEndResponse>> {
        todo_response(request)
    }

    async fn pause_timer(
        &self,
        request: Request<PauseTimerRequest>,
    ) -> StatusResult<Response<PauseTimerResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let user = auth::user_from_request(&request);

        use crate::timers::dsl::*;
        let rows = diesel::update(timers.find(Uuid::parse_str(&message.uuid).unwrap()))
            .filter(owner_predicate(user))
            .set((
                    elapsed_before_start.eq(diesel::dsl::sql("elapsed_before_start + COALESCE(CURRENT_TIMESTAMP - start_time, '0 seconds')")),
                    start_time.eq(None::<NaiveDateTime>),
            ))
            .execute(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        if let Some(err) = self.notify_timer_change(user.map(|user| vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }

        if rows >= 1 {
            Ok(Response::new(PauseTimerResponse {}))
        } else {
            Err(tonic::Status::not_found("Timer not found"))
        }
    }

    async fn resume_timer(
        &self,
        request: Request<ResumeTimerRequest>,
    ) -> StatusResult<Response<ResumeTimerResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let user = auth::user_from_request(&request);

        let rows = diesel::update(timers::table.find(Uuid::parse_str(&message.uuid).unwrap()))
            .filter(owner_predicate(user))
            .set((timers::start_time.eq(diesel::dsl::now),))
            .execute(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        if let Some(err) = self.notify_timer_change(user.map(|user| vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }

        if rows >= 1 {
            Ok(Response::new(ResumeTimerResponse {}))
        } else {
            Err(tonic::Status::not_found("Timer not found"))
        }
    }

    async fn get_timer_name(
        &self,
        request: Request<GetTimerNameRequest>,
    ) -> StatusResult<Response<GetTimerNameResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let (name, owner) = timers::table
            .find(Uuid::try_parse(&message.uuid).unwrap())
            .select((timers::title, timers::owner))
            .get_result(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        should_allow_access(auth::user_from_request(&request), owner)
            .then_some(Response::new(GetTimerNameResponse { name }))
            .ok_or(tonic::Status::permission_denied("User does not own this timer."))
    }

    async fn set_timer_name(
        &self,
        request: Request<SetTimerNameRequest>,
    ) -> StatusResult<Response<SetTimerNameResponse>> {
        let user = auth::user_from_request(&request);

        let message = request.into_inner();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let timer = timers::table
            .find(Uuid::try_parse(&message.uuid).map_err(|err| tonic::Status::invalid_argument(err.to_string()))?);

        let rows = diesel::update(timer)
            .filter(owner_predicate(user))
            .set(timers::title.eq(message.new_name))
            .execute(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        if let Some(err) = self.notify_timer_change(user.map(|user| vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }

        if rows == 1 {
            Ok(Response::new(SetTimerNameResponse {}))
        } else {
            Err(tonic::Status::not_found("Timer not found"))
        }
    }

    async fn extend_timer(
        &self,
        request: Request<ExtendTimerRequest>,
    ) -> StatusResult<Response<ExtendTimerResponse>> {
        todo_response(request)
    }

    async fn shorten_timer(
        &self,
        request: Request<ShortenTimerRequest>,
    ) -> StatusResult<Response<ShortenTimerResponse>> {
        todo_response(request)
    }

    async fn delete_timer(
        &self,
        request: Request<DeleteTimerRequest>,
    ) -> StatusResult<Response<DeleteTimerResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let user = auth::user_from_request(&request);

        diesel::delete(timers::table.find(Uuid::parse_str(&message.uuid).unwrap()))
            .filter(owner_predicate(user))
            .execute(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        if let Some(err) = self.notify_timer_change(user.map(|user| vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }
        Ok(Response::new(DeleteTimerResponse {}))
    }

    async fn reset(&self, request: Request<ResetRequest>) -> StatusResult<Response<ResetResponse>> {
        let message = request.get_ref();

        let mut db_conn = self
            .db_pool
            .get()
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let user = auth::user_from_request(&request);

        let rows = diesel::update(timers::table.find(Uuid::parse_str(&message.uuid).unwrap()))
            .filter(owner_predicate(user))
            .set((
                timers::start_time.eq(None::<NaiveDateTime>),
                timers::elapsed_before_start.eq(PgInterval::from_microseconds(0)),
            ))
            .execute(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        if let Some(err) = self.notify_timer_change(user.map(|user| vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }

        if rows == 1 {
            Ok(Response::new(ResetResponse {}))
        } else {
            Err(tonic::Status::not_found("Timer not found"))
        }
    }

    async fn forward(
        &self,
        request: Request<ForwardRequest>,
    ) -> StatusResult<Response<ForwardResponse>> {
        todo_response(request)
    }

    async fn rewind(
        &self,
        request: Request<RewindRequest>,
    ) -> StatusResult<Response<RewindResponse>> {
        todo_response(request)
    }

    async fn set_total(
        &self,
        request: Request<SetTotalRequest>,
    ) -> StatusResult<Response<SetTotalResponse>> {
        todo_response(request)
    }

    async fn new_timer_ending_lasting_duration_at_time(
        &self,
        request: Request<NewTimerEndingLastingDurationAtTimeRequest>,
    ) -> StatusResult<Response<NewTimerEndingLastingDurationAtTimeResponse>> {
        todo_response(request)
    }

    async fn new_timer_lasting(
        &self,
        request: Request<NewTimerLastingRequest>,
    ) -> StatusResult<Response<NewTimerLastingResponse>> {
        let message = request.get_ref();

        let mut db_conn = self.db_pool.get().map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;
        
        let user = auth::user_from_request(&request)
            .ok_or(tonic::Status::unauthenticated("Unauthenticated"))?;

        let uuid: Uuid = Timer::new_paused("", message.total, user)
            .insert_into(timers::table)
            .returning(timers::uuid)
            .get_result(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let uuid = uuid.to_string();

        if let Some(err) = self.notify_timer_change(Some(vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }
        Ok(Response::new(NewTimerLastingResponse { uuid }))
    }

    async fn new_timer_lasting_with_remaining(
        &self,
        request: Request<NewTimerLastingWithRemainingRequest>,
    ) -> StatusResult<Response<NewTimerLastingWithRemainingResponse>> {
        let message = request.get_ref();

        let mut db_conn = self.db_pool.get().map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;
        
        let user = auth::user_from_request(&request)
            .ok_or(tonic::Status::unauthenticated("Unauthenticated"))?;

        let uuid: Uuid = Timer::new_paused_with_remaining("", message.total, message.remaining, user)
            .insert_into(timers::table)
            .returning(timers::uuid)
            .get_result(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let uuid = uuid.to_string();

        if let Some(err) = self.notify_timer_change(Some(vec![user])).await.err() {
            std::eprintln!("Error while sending timer-change notification: {err}");
        }
        Ok(Response::new(NewTimerLastingWithRemainingResponse { uuid }))
    }

    type TimerStatusesStream = tokio_stream::wrappers::ReceiverStream<StatusResult<eyesheet::chronch::TimerStatus>>;//tokio_wrappers::stream::ReceiverStream<StatusResult<eyesheet::chronch::TimerStatus>>;

    async fn timer_statuses(&self, request: tonic::Request<()>) -> Result<tonic::Response<Self::TimerStatusesStream>, tonic::Status> {
        let mut db_conn = self.db_pool.get().map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let selection = {
            use crate::timers::dsl::*;
            (uuid, title, duration, elapsed_before_start, start_time)
        };

        let results = timers::table
            .select(selection)
            .filter(owner_predicate(auth::user_from_request(&request)))
            .get_results::<(Uuid, String, PgInterval, PgInterval, Option<NaiveDateTime>)>(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;
        let statuses = results.into_iter().map(|(uuid, title, duration, elapsed_before_start, start_time)| {
            TimerStatus {
                uuid: uuid.to_string(),
                name: title,
                total: duration.microseconds,
                tail: match start_time {
                    Some(start_time) => Some(timer_status::Tail::End(
                            start_time.timestamp_micros() + duration.microseconds - elapsed_before_start.microseconds
                            )),
                    None => Some(timer_status::Tail::Remaining(duration.microseconds - elapsed_before_start.microseconds)),
                },
            }
        });

        let (tx, rx) = tokio::sync::mpsc::channel(128);
        tokio::spawn(async move {
            statuses
                .map(|status| tx.send(Ok(status)))
                .collect::<FuturesUnordered<_>>()
                .try_collect::<()>()
                .await
                .unwrap();
        });

        Ok(Response::new(Self::TimerStatusesStream::new(rx)))
    }

    async fn timer_statuses_once( &self, request: tonic::Request<()>) -> Result<tonic::Response<RepeatedTimerStatus>, tonic::Status> {
        let mut db_conn = self.db_pool.get().map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;

        let selection = {
            use crate::timers::dsl::*;
            (uuid, title, duration, elapsed_before_start, start_time)
        };

        let results = timers::table
            .select(selection)
            .filter(owner_predicate(auth::user_from_request(&request)))
            .get_results::<(Uuid, String, PgInterval, PgInterval, Option<NaiveDateTime>)>(&mut db_conn)
            .map_err(|err| tonic::Status::internal(format!("DB error {err}")))?;
        let statuses = results.into_iter().map(|(uuid, title, duration, elapsed_before_start, start_time)| {
            TimerStatus {
                uuid: uuid.to_string(),
                name: title,
                total: duration.microseconds,
                tail: match start_time {
                    Some(start_time) => Some(timer_status::Tail::End(
                            start_time.timestamp_micros() + duration.microseconds - elapsed_before_start.microseconds
                            )),
                    None => Some(timer_status::Tail::Remaining(duration.microseconds - elapsed_before_start.microseconds)),
                },
            }
        }).collect();

        Ok(Response::new(RepeatedTimerStatus { statuses }))
    }
}

fn should_allow_access(user: Option<Uuid>, owner: Uuid) -> bool {
    user.map_or(owner.is_nil(), |user| user == owner)
}

fn owner_predicate(user: Option<Uuid>) -> diesel::helper_types::EqAny<timers::owner, Vec<Uuid>> {
    return timers::owner.eq_any([Uuid::nil()].into_iter().chain(user.into_iter()).collect::<Vec<_>>());
}

fn todo_response<Request: std::fmt::Debug, Response>(request: tonic::Request<Request>) -> Result<tonic::Response<Response>, tonic::Status> {
    let message = request.get_ref();
    Err(tonic::Status::unimplemented(format!("TODO. Received: {message:?}")))
}

#[derive(Insertable)]
struct Timer<'a> {
    title: &'a str,
    duration: PgInterval,
    elapsed_before_start: Option<PgInterval>,
    start_time: Option<chrono::NaiveDateTime>,
    owner: Uuid,
}

impl<'a> Timer<'a> {
    fn new_paused(title: &'a str, duration_micros: i64, owner: Uuid) -> Self {
        Self { title, duration: PgInterval::from_microseconds(duration_micros), elapsed_before_start: None, start_time: None, owner }
    }

    fn new_paused_with_remaining(title: &'a str, duration_micros: i64, remaining_micros: i64, owner: Uuid) -> Self {
        Self { title, duration: PgInterval::from_microseconds(duration_micros), elapsed_before_start: Some(PgInterval::from_microseconds(duration_micros - remaining_micros)), start_time: None, owner }
    }

}

fn action_trigger_time(metadata: &tonic::metadata::MetadataMap) -> Option<chrono::DateTime<chrono::Utc>> {
    let value = metadata.get("x-action-timestamp")?;
    let s = value.to_str().ok()?;
    chrono::DateTime::parse_from_rfc3339(s)
        .map(Into::into)
        .ok()
}

struct ActionTimestamp(chrono::DateTime<chrono::Utc>);

fn interceptor(mut request: Request<()>) -> StatusResult<Request<()>> {
    if let Some(action_timestamp) = action_trigger_time(request.metadata()) {
        request.extensions_mut().insert(ActionTimestamp(action_timestamp));
    }

    auth::insert_token_in_extensions(&mut request)?;

    Ok(request)
}

fn request_timestamp(extensions: &tonic::Extensions) -> Option<chrono::DateTime<chrono::Utc>> {
    extensions
        .get::<ActionTimestamp>()
        .map(|v| v.0)
}

fn request_timestamp_or_now(extensions: &tonic::Extensions) -> chrono::DateTime<chrono::Utc> {
    request_timestamp(extensions)
        .unwrap_or(chrono::Utc::now())
}

#[derive(Debug)]
struct EnvVarNotUnicode;

impl std::fmt::Display for EnvVarNotUnicode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "environment variable was not unicode")
    }
}

impl std::error::Error for EnvVarNotUnicode {}

fn env_var_opt<K: AsRef<OsStr>>(key: K) -> Result<Option<String>, EnvVarNotUnicode> {
    use std::env::VarError::*;
    std::env::var(key)
        .map(Some)
        .or_else(|err| match err {
            NotPresent => Ok(None),
            NotUnicode(_) => Err(EnvVarNotUnicode),
        })
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    dotenv().ok();

    let service_addr_env_var = "CHRONCH_SERVICE_ADDR";
    let addr = std::env::var(service_addr_env_var)
        .expect(&format!("Environment variable \"{}\" was not set.", service_addr_env_var))
        .parse()
        .expect(&format!("Environment variable \"{}\" was not an address.", service_addr_env_var));

    let kafka_config = env_var_opt("KAFKA_URL").ok().flatten().map(|url| {
        let mut kafka_config = rdkafka::ClientConfig::new();
        kafka_config
            .set("bootstrap.servers", url)
            .set("group.id", "chronch")
            .set("queue.buffering.max.ms", 1.to_string());
        kafka_config
    });

    let kafka_producer = kafka_config
        .map(|c| c.create())
        .transpose()?;

    let db_url = std::env::var("DATABASE_URL")?;
    let manager = ConnectionManager::<PgConnection>::new(&db_url);
    let db_pool = Pool::builder()
        .build(manager)?;
    db_pool.get()?.run_pending_migrations(MIGRATIONS)?;
    let chronch_service = ChronchService {
        db_pool,
        kafka_producer,
    };

    std::println!("Starting server on {addr}");
    
    Server::builder()
        .accept_http1(true)
        .layer((
                tower_http::cors::CorsLayer::very_permissive(),
                tonic_web::GrpcWebLayer::new())
            )
        .add_service(ChronchServer::with_interceptor(chronch_service, interceptor))
        .serve(addr)
        .await?;

    Ok(())
}
