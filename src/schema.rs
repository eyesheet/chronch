// @generated automatically by Diesel CLI.

diesel::table! {
    timers (uuid) {
        uuid -> Uuid,
        title -> Varchar,
        duration -> Interval,
        elapsed_before_start -> Interval,
        start_time -> Nullable<Timestamp>,
        owner -> Uuid,
    }
}
