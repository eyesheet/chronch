fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=migrations");
    tonic_build::configure()
        .build_client(false)
        .compile(
            &["proto/chronch.proto"],
            &["proto"],
            )?;
    Ok(())
}
